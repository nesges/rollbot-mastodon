<?
    $version=0.2;
    
    include("config.php");
    include("inc/libraries.php");
    if(isset($_COOKIE['rollbot-clientid'])) {
        $rollbot_clientid = $_COOKIE['rollbot-clientid'];
    } else {
        $rollbot_clientid = "rollbot-".bin2hex(random_bytes(16));
        setcookie('rollbot-clientid', $rollbot_clientid);
    }
?>
<html>
    <head>
        <title>rollbot &#129302;</title>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Ein rollender Mastodon Bot">
        
        <meta name="application-name" content="rollbot">
        <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-touch-icon.png?v=<?= $version ?>">
        <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png?v=<?= $version ?>">
        <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png?v=<?= $version ?>">
        <link rel="manifest" href="favicon/site.webmanifest">
        
        <meta name="fediverse:creator" content="dnddeutsch@pnpde.social" />
        
        <!-- jQuery -->
        <?= cachebusted("vendor/components/jquery/jquery.min.js") ?>
        
        <!-- bootstrap -->
        <?= cachebusted("lib/bootstrap/js/bootstrap.bundle.min.js") ?>
        <?= cachebusted("lib/bootstrap-icons/bootstrap-icons.css") ?>
        
        <!-- bootswatch: sandstone theme -->
        <!-- ?= cachebusted("lib/bootswatch/sandstone/bootstrap.min.css") ?-->
        <?= cachebusted("css/bootstrap.min.css") ?>
        
        <!-- fontawesome -->
        <?= cachebusted("lib/fontawesome/css/all.min.css") ?>
        <?= cachebusted("lib/fontawesome/js/all.min.js") ?>
        
        <!-- timeline -->
        <?= cachebusted("js/mastodon-embed-feed-timeline/mastodon-timeline.css") ?>
        <?= cachebusted("js/mastodon-embed-feed-timeline/mastodon-timeline.js") ?>
        
        <!-- socket.io -->
        <?= cachebusted("js/socket.io.js") ?>
        
        <!-- rollbot -->
        <?= cachebusted("css/rollbot.css") ?>
        <?= cachebusted("js/rollbot.js") ?>
        <script>
            const clientid = '<?= $rollbot_clientid ?>';

            const creature = [ 
                <?
                    $creatures = [];
                    if ($dir = opendir('img/creature')) {
                        while (false !== ($file= readdir($dir))) {
                            if(preg_match('/\.svg$/', $file)) {
                                $creatures[] = $file;
                            }
                        }
                        closedir($dir);
                    }
                    print "'".join("','", $creatures)."'";
                ?>
            ];
            
            jQuery(document).ready(function() {
                let mapi = new MastodonApi({
                    container_body_id: "mt-body",
                    default_theme: "light",
                    instance_url: "https://<?= $config['mastodon']['domain'] ?>",
                    timeline_type: "profile",
                    user_id: "<?= $config['mastodon']['user_id'] ?>",
                    profile_name: "roll",
                    hashtag_name: "",
                    toots_limit: "5",
                    hide_unlisted: false,
                    hide_reblog: false,
                    hide_replies: false,
                    text_max_lines: "0",
                    link_see_more: "Mehr Rolls auf Mastodon",
                });
            });           
        </script>
    </head>
    <body>
        <div class="container-fluid" style="z-index:1">
            <div class="row">
                <div class="col col-xl-3 col-md-4 col-sm-0" style="background-color:var(--d3-red); z-index:1">
                    <div class="sidebar text-center p-3 pt-5 sticky-top">
                        
                        <div id="carousel-sidebar" class="carousel slide">
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <h1>rollbot &#129302;</h1>
                                    
                                    <p class="mt-4">rollbot ist ein Mastodon-Bot, der rollt.</p>
                                    
                                    <p class="mt-5 text-start">Und zwar W&uuml;rfel. Du brauchst nur einen Status zu posten, in dem eine W&uuml;rfelformel enthalten ist, und <a href="https://mastodon.pnpde.social/@roll">@roll@pnpde.social</a> darin erw&auml;hnen. rollbot wird sich kurz darauf mit dem gew&uuml;rfelten Ergebnis melden.</p>
                                    
                                    <p class="mt-3 text-start">rollbot verwendet dazu <a href="https://github.com/dice-roller/rpg-dice-roller">RPG Dice Roller</a> und versteht seine W&uuml;rfelnotation. Du musst nichts Neues lernen, es ist die W&uuml;rfelnotation, die du bestimmt schonmal gesehen hast. Sowas wie <strong title="zwei zwangigseitige W&uuml;rfel">2W20</strong> oder <strong title="vier sechsseitige W&uuml;rfel, ignoriere (d=drop) den niedrigsten">4W6d1</strong> oder auch komplexere Formeln, wie sie <a href="https://dice-roller.github.io/documentation/guide/notation/">in der Dokumentation beschrieben</a> sind.
                                    
                                    <p class="mt-3 text-start">PS: rollbot favorisiert deinen Status, sobald er ihn gesehen hat. Wenn innerhalb weniger Minuten nach dem Favorisieren keine Antwort kommt, ist etwas schief gelaufen</p>
                                    
                                    <p class="mt-5 text-center">ein Projekt von <a class="imglink d3-logo" href="https://www.dnddeutsch.de"><? readfile('img/d3.svg') ?></a></p>
                                    
                                    <a href="https://www.dnddeutsch.de/ai-art-auf-d3/" style="background-color:transparent !important">
                                        <div style="height:60px; margin-top:40px">
                                        <?
                                            $svg = file_get_contents('/var/www/lib/human-made-icons/hm_vertical_nobox_white.svg');
                                            $svg = preg_replace('#<!DOCTYPE svg.*?>#', '', $svg);
                                            $svg = preg_replace('#<\?xml.*?\?>#', '', $svg);
                                            print $svg;
	                                    ?>
                                        </div>
                                    </a>
                                </div>
                                <div class="carousel-item">
                                    <h1>Web</h1>
                                    
                                    <p class="mt-4">rollbot ist eine Webseite, die rollt.</p>
                                    
                                    <p class="mt-5 text-start">Mit den Würfelbuttons kannst du die Würfel rollen lassen. Aber diese Würfe sind nicht nur für dich, sondern für alle, die die Website zur gleichen Zeit offen haben. Alle sehen deine Würfe und umgekehrt!</p>
                                    
                                    <p class="mt-3 text-start"><strong>Wer bist du?</strong> Alle Besucher&ast;innen erhalten eine einzigartige Kombination aus drei Icons und einer Farbe. Ihr müsst euch deshalb nicht anmelden, seid pseudonym und könnt euch trotzdem darüber abstimmen, wer welche Ergebnisse erwürfelt hat: <em></em>"Ich bin evil-minion read drink-me!"</em> Klicke auf eine Icon-Kombination, um ihre Textversion anzuzeigen.</p>
                                    
                                    <p class="mt-3 text-start"><strong>Raus aus der lobby!</strong> Wenn ihr euch als Gruppe zurückziehen möchtet, könnt ihr eigene Chaträume eröffnen. Alle starten mit ihrem ersten Wurf in der <strong><i class="fa-solid fa-people-group"></i> lobby</strong>. Doppelklicke auf das Wort <strong><i class="fa-solid fa-people-group"></i> lobby</strong> und gib einen neuen Namen ein. Alle, die den gleichen Namen eingeben, betreten den gleichen Raum und sehen nur noch Würfel, die darin geworfen werden. Dort könnt ihr auch den X-Karte-Button benutzen.</p>
                                    
                                    <p class="mt-3 text-start"><strong>Privacy by Design:</strong> Der Chat wird nicht auf dem Server gespeichert und geht verloren, wenn der Browser geschlossen oder aktualisiert wird.</p>
                                </div>
                                <div class="carousel-item">
                                    <h1>Pixels</h1>
                                    
                                    <p class="mt-4">rollbot ist ein Pixels-Interface.</p>
                                    
                                    <? 
                                        $html = file_get_contents("pixels.html");
                                        $html = preg_replace('#<html>.*?<body>#s', '', $html);
                                        $html = preg_replace('#<h1.*?</h1>#s', '', $html);
                                        $html = preg_replace('#<div class="footer.*?</html>#s', '', $html);
                                        
                                        print $html;
                                    ?>
                                </div>
                          </div>
                          <button class="carousel-control-prev" type="button" data-bs-target="#carousel-sidebar" data-bs-slide="prev" style="align-self: flex-start">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Zurück</span>
                          </button>
                          <button class="carousel-control-next" type="button" data-bs-target="#carousel-sidebar" data-bs-slide="next" style="align-self: flex-start">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Weiter</span>
                          </button>
                        </div>

                    </div>
                </div>
                <div class="col col-xl-9 col-md-8 col-sm-12">
                    <div class="m-1 position-fixed top-0 end-0" style="z-index:2">
                        <a rel="me" href="https://mastodon.pnpde.social/@roll"><img class="icon m-2" src="img/mastodon_colored.png?v=<?= $version ?>"></a>
                    </div>
                    
                    <div class="m-1 position-fixed top-0 end-0" style="z-index:2">
                        <div class="pixels-logo" title="[TESTING] Pixels enabled. Click for instructions">
                            <a href="https://rollbot.dnddeutsch.de/pixels">
                            <?
                                $svg = file_get_contents('img/pixels.svg');
                                $svg = preg_replace('#<!DOCTYPE svg.*?>#', '', $svg);
                                $svg = preg_replace('#<\?xml.*?\?>#', '', $svg);
                                print $svg;
	                        ?>
	                        </a>
	                    </div>
                    </div>
                    
                    <div class="m-3 content">
                        <div class="text-center m-4">
                            <img title="1W20" class="die m-2" data-formula="1d20" src="img/diceicon/blank_d20_red.png?v=<?= $version ?>">
                            <img title="1W12" class="die m-2" data-formula="1d12" src="img/diceicon/blank_d12_red.png?v=<?= $version ?>">
                            <img title="1W10" class="die m-2" data-formula="1d10" src="img/diceicon/blank_d10_red.png?v=<?= $version ?>">
                            <img title="1W8"  class="die m-2" data-formula="1d8"  src="img/diceicon/blank_d8_red.png?v=<?= $version ?>">
                            <img title="1W6"  class="die m-2" data-formula="1d6"  src="img/diceicon/blank_d6_red.png?v=<?= $version ?>">
                            <img title="1W4"  class="die m-2" data-formula="1d4"  src="img/diceicon/blank_d4_red.png?v=<?= $version ?>">
                            <img title="W&uuml;rfelformeln"     class="dicetable m-2" src="img/diceicon/perspective-dice-six-faces-random-red.png?v=<?= $version ?>">
                        </div>
                        
                        <div class="formulaform text-center d-flex flex-fill justify-content-center" style="margin-left:auto; margin-right:auto; left:0; right:0">
                            <input type="text" class="flex-fill" placeholder="2w20" id="formula">
                            <span id="btnRoll" class="" title="roll!"><i class="fa-solid fa-dice-d6"></i></span>
                        </div>
                        
                        <div class="mt-3 text-center" id="diceroll" style="margin-left:auto; margin-right:auto; left:0; right:0"></div>
                        
                        <div class="text-center">
                            <div class="container my-3" id="multidice" >
                                <div class="row mt-3">
                                    <div title="2W20 mit Vorteil" class="col die m-3 text-nowrap" data-formula="2d20dl1 # mit Vorteil">
                                        <strong>Vorteil</strong><br>
                                        <img src="img/diceicon/blank_d20_red.png?v=<?= $version ?>" style="filter: saturate(0)">
                                        <img src="img/diceicon/blank_d20_red.png?v=<?= $version ?>" style="margin-left: -20px; transform:rotate(15deg)">
                                    </div>
                                
                                    <div title="2W20 mit Nachteil" class="col die m-3 text-nowrap" data-formula="2d20dh1 # mit Nachteil">
                                        <strong>Nachteil</strong><br>
                                        <img src="img/diceicon/blank_d20_red.png?v=<?= $version ?>">
                                        <img src="img/diceicon/blank_d20_red.png?v=<?= $version ?>" style="margin-left: -20px; transform:rotate(15deg); filter: saturate(0)">
                                    </div>
                                    
                                    <div title="1W100" class="col die m-3 text-nowrap" data-formula="1d100 # Prozent">
                                        <strong>1W100</strong><br>
                                        <img src="img/diceicon/blank_d10_red.png?v=<?= $version ?>">
                                    </div>
                                
                                    <div title="4W6 ohne den niedrigsten" class="col die m-3 text-nowrap" data-formula="4d6d1 # Attributwert">
                                        <strong>1 Attributwert</strong><br>
                                        <img src="img/diceicon/blank_d6_red.png?v=<?= $version ?>" style="filter: saturate(0)">
                                        <img src="img/diceicon/blank_d6_red.png?v=<?= $version ?>" style="margin-left: -20px; transform:rotate(5deg)">
                                        <img src="img/diceicon/blank_d6_red.png?v=<?= $version ?>" style="margin-left: -20px; transform:rotate(10deg)">
                                        <img src="img/diceicon/blank_d6_red.png?v=<?= $version ?>" style="margin-left: -20px; transform:rotate(15deg)">
                                    </div>
                                    
                                    <div title="sechs mal 4W6 ohne den niedrigsten" class="col die m-3 text-nowrap" data-formula="4d6d1 # Attributwert" data-repeat="6">
                                        <strong>6 Attributwerte</strong><br>
                                        6 X
                                        <img src="img/diceicon/blank_d6_red.png?v=<?= $version ?>" style="filter: saturate(0)">
                                        <img src="img/diceicon/blank_d6_red.png?v=<?= $version ?>" style="margin-left: -20px; transform:rotate(5deg)">
                                        <img src="img/diceicon/blank_d6_red.png?v=<?= $version ?>" style="margin-left: -20px; transform:rotate(10deg)">
                                        <img src="img/diceicon/blank_d6_red.png?v=<?= $version ?>" style="margin-left: -20px; transform:rotate(15deg)">
                                    </div>
                                </div>
                                
                                <div class="row mt-2">
                                    <div class="col fw-bold">1</div>
                                    <div class="col fw-bold">2</div>
                                    <div class="col fw-bold d-none d-xl-block d-lg-block d-md-block">3</div>
                                    <div class="col fw-bold d-none d-xl-block d-lg-block">4</div>
                                    <div class="col fw-bold d-none d-xl-block">5</div>
                                </div>
                            <?
                                foreach([20,12,10,8,6,4] as $sides) {
                                    ?><div class="row mt-2"><?
                                    for($c=1;$c<=5;$c++) {
                                        ?>
                                        <div title="<?= $c ?>W<?=$sides?>" class="col die mx-2 text-nowrap 
                                            <?= $c==3 ? "d-none d-xl-block d-lg-block d-md-block" : "" ?> 
                                            <?= $c==4 ? "d-none d-xl-block d-lg-block" : "" ?>
                                            <?= $c==5 ? "d-none d-xl-block" : "" ?>
                                            " data-formula="<?= $c ?>d<?=$sides?>">
                                            <?
                                                for($i=1;$i<=$c;$i++) {
                                                    ?>
                                                    <img src="img/diceicon/blank_d<?=$sides?>_red.png?v=<?= $version ?>" <?= $i>1 ? 'style="margin-left: -25px"' : '' ?>>
                                                    <?
                                                }
                                            ?>
                                        </div>
                                        <?
                                    }
                                    ?></div><?
                                }
                            ?>
                            </div>
                        </div>
                        
                        <div id="log" class="log mb-3 position-relative justify-content-center">
                            <div style="width:fit-content" class="loglines position-relative pt-2">
                                <span class="badge bg-secondary position-absolute top-0 end-0">lobby</span>
                                <div class="rolls border p-3"></div>
                            </div>
                        </div>

                        <div id="btnRegisterPanel" class="text-center"><i class="fa-solid fa-people-group"></i></div>
                        <div id="register" class="text-center" style="margin-left:auto; margin-right:auto; left:0; right:0; width:fit-content">
                            <div id="room" class="me-4" title="Doppelklick zum Bearbeiten und Raum wechseln">
                                <span id="btnX" class="me-3 text-light" title="X-Karte zeigen">
                                    <i class="fa-solid fa-square-xmark"></i>
                                </span>
                                
                                <i class="fa-solid fa-people-group"></i> 
                                <span id="roomname">
                                    <div class="spinner-border spinner-border-sm" role="status">
                                        <span class="visually-hidden">verbinde...</span>
                                    </div>
                                </span>
                            </div>
                            <div id="registerid" title="andere erkennen deine W&uuml;rfe an dieser ID">
                                <i class="fa-solid fa-address-card"></i>
                                <span id="register-icons">
                                    <div class="spinner-border spinner-border-sm" role="status">
                                        <span class="visually-hidden">verbinde...</span>
                                    </div>
                                </span>
                            </div>
                            <span id="receiveRolls" title="W&uuml;rfe empfangen?" class="text-success fa-stack m-0 p-0"><i class="p-0 fa-solid fa-left-long  fa-stack-1x"></i></span>
                            <span id="sendRolls"    title="W&uuml;rfe senden?"    class="text-success fa-stack m-0 p-0"><i class="p-0 fa-solid fa-right-long fa-stack-1x"></i></span>
                                <span id="connection" class="me-3 text-danger">
                                    <i class="fa-solid fa-plug-circle-xmark"></i>
                                </span>
                        </div>
                        
                        <div class="mt-0 mt-timeline" style="margin-left:auto; margin-right:auto; left:0; right:0">
                            <div id="mt-body" class="mt-body" role="feed">
                                <div class="loading-spinner"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            
        <div class="footer p-2 m-0 position-sticky bottom-0 left-0" style="z-index:2">
            <div class="col">
                <div class="text-center">
                    powered by <a href="https://github.com/dice-roller/rpg-dice-roller">RPG Dice Roller</a>, <a href="https://game-icons.net">Game-icons.net</a>, <a href="https://socket.io/">Socket.io</a> and <a href="https://gitlab.com/idotj/mastodon-embed-feed-timeline">Mastodon timeline feed widget</a> &#10084;&#65039;
                    // Sourcecode <a href="https://codeberg.org/nesges/rollbot-mastodon">on Codeberg</a>
                    // <a rel="me" href="https://mastodon.pnpde.social/@roll"><i class="fa-brands fa-mastodon"></i> Mastodon</a>
                    // <a href="https://dnddeutsch.de/kontakt/"><i class="fa-solid fa-envelope"></i> Kontakt</a>
                    // <a href="https://dnddeutsch.de/impressum/"><i class="fa-solid fa-section"></i> Impressum</a>
                </div>
            </div>
        </div>
        
        <div class="modal" tabindex="-1" id="modalViewer">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Schlie&szlig;en"></button>
                    </div>
                    <div class="modal-body align-middle text-center">
                        <div class="icons"></div>
                        <p class="mt-3">Jede Anwender&ast;in der rollbot-Website hat eine solche ID. Sie hilft euch, eure W&uuml;rfe einander zuzuordnen und sch&uuml;tzt dabei eure Identit&auml;t. rollbot vergisst IDs, die 24 Stunden nicht benutzt wurden und dein Browser vergisst seine ID, wenn du das Browserfenster schlie&szlig;t.</p>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="toast-container position-fixed bottom-0 end-0 p-3">
            <div id="toast" class="toast" role="alert" aria-live="assertive" aria-atomic="true">
                <div class="toast-header">
                    <strong class="toast-title me-auto text-primary"></strong>
                    <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Schlie&szlig;en"></button>
                </div>
                <div class="toast-body"></div>
            </div>
        </div>
        
        <div id="xcard" class="modal" tabindex="-1">
            <div id="xcard" class="modal-dialog modal-sm">
                <div class="modal-content text-bg-danger">
                    <div class="modal-body fw-bold text-center p-5">
                        <div style="font-size:200px">
                            <i class="fa-solid fa-x"></i>
                        </div>    
                    </div>
                    <div class="modal-footer">
                        <p>Jemand benutzt die X-Karte und bittet darum einen Spielinhalt zu entfernen.</p>
                        <p><a class="text-light" href="https://www.dnddeutsch.de/xcard/">Hier kannst du dich &uuml;ber die X-Karte informieren</a>.</p>
                        <p><strong>Wenn etwas mit der X-Karte markiert wird, ist keine Rechtfertigung n&ouml;tig.</strong></p>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
<?
    function cachebusted($filename, $type="") {
        global $config;
        
        if(!$type) {
            if(preg_match('/\.js$/', $filename)) {
                $type = 'js';
            } else if(preg_match('/\.css$/', $filename)) {
                $type = 'css';
            }
        }
        
        $localfile = dirname(__FILE__).'/'.$filename;
        
        if(file_exists($localfile)) {
            $v = md5($config['salt'].filemtime($localfile));
            
            if($type=='js') {
                return '<script src="'.$filename.'?v='.$v.'"></script>'."\n";
            } else if($type=='css') {
                return '<link rel="stylesheet" href="'.$filename.'?v='.$v.'" type="text/css">'."\n";
            } else {
                return $filename.'?v='.$v;
            }
        } else {
            return $filename.'?v=notfound';
        }
    }
?>
