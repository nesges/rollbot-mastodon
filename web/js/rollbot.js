var socket = io('wss://io.dnddeutsch.de');
var sender = [];
var register = [];
var room = 'lobby';
var chatLineNr = 0;

jQuery(document).ready(function() {
    var rollbot = $(window);

    $('#log').hide();
    $('#register').hide();
    $('#multidice').hide();

    socket.on('connect', function(data) {
        $('#connection').removeClass('text-danger');
        $('#connection').removeClass('text-warning');
        $('#connection').addClass('text-success');
        $('#connection svg').remove();
        $('#connection i.fa-solid').remove();
        $('#connection').append('<i class="fa-solid fa-plug-circle-check"></i>');
        $('#connection').attr('title', 'Chat-Verbindung OK');
    
        socket.emit('register', { 'sender': clientid });    
        
        if(room) {
            socket.emit('join', room);
        } else {
            socket.emit('join', 'lobby');
        }
    });
    
    socket.on('disconnect', function(data) {
        $('#connection').addClass('text-warning');
        $('#connection').removeClass('text-danger');
        $('#connection').removeClass('text-success');
        $('#connection svg').remove();
        $('#connection i.fa-solid').remove();
        $('#connection').append('<i class="fa-solid fa-plug-circle-exclamation"></i>');
        $('#connection').attr('title', 'Chat-Verbindung unterbrochen');
    });
    
    socket.on('connect_error', function(data) {
        $('#connection').addClass('text-danger');
        $('#connection').removeClass('text-warning');
        $('#connection').removeClass('text-success');
        $('#connection svg').remove();
        $('#connection i.fa-solid').remove();
        $('#connection').append('<i class="fa-solid fa-plug-circle-xmark"></i>');
        $('#connection').attr('title', 'Chat-Verbindung fehlerhaft');
    });
    
    socket.on('register-create', function(data) {
        register[data.register_id] = data;
        $('#register-icons').html(registeredIcons(data.register_id));
    });
    socket.on('register-known', function(data) {
        register[data.register_id] = data;
        $('#register-icons').html(registeredIcons(data.register_id));
    });
    socket.on('whois', function(data) {
        register[data.register_id] = data;
        rollbot.trigger('whois', data);
    });

    socket.on('joined', function(data) {
        room = data;
        $('#roomname').html(room);
        $('#log .badge').html(room);
        
        if(room != 'lobby') {
            $('#btnX').removeClass('text-light');
            $('#btnX').addClass('text-danger');
        } else {
            $('#btnX').addClass('text-light');
            $('#btnX').removeClass('text-danger');
        }
    });

    socket.on('chat', function(data) {
        $('#log').show();
        $('#register').show();
        $('#btnRegisterPanel').hide();
        
        const receive = $('#receiveRolls').hasClass('text-success');
        
        if(receive || data.register_id == clientid) {
            $('#register').show();
            $('#btnRegisterPanel').hide();
            $('#log').show();
            $('#log').addClass('d-flex');
            
            const d = new Date(data.time);
            let time = (''+d.getHours()).padStart(2, '0') + ':' + (''+d.getMinutes()).padStart(2, '0') + ':' + (''+d.getSeconds()).padStart(2, '0');
            
            var chatline = $('<div class="roll" data-line-nr="'+(data.line_nr ? data.line_nr : ++chatLineNr)+'"><small>'+time+'</small> </div>');
            if(register[data.register_id]) {
                chatline.append(registeredIcons(data.register_id));
                chatline.append(data.message);
                if(data.line_nr && $('[data-line-nr='+data.line_nr+']').length) {
                    $('[data-line-nr='+data.line_nr+']').replaceWith(chatline);
                } else {
                    $('#log .rolls').prepend(chatline);
                }
            } else {
                whois(data.register_id, function(register_id) {
                    chatline.append(registeredIcons(register_id));
                    chatline.append(data.message);
                    $('#log .rolls').prepend(chatline);
                });
            }
        }
    });
    
    socket.on('x', function(data) {
        const modal = new bootstrap.Modal('#xcard');
        modal.show();
    });
    
    
    $('#formula').on('keypress', function(event){
        if(event.which == 13) {
            $('#btnRoll').click();
        }
    });
    
    $('#btnRoll').click(function() {
        var formula = $('#formula').val();
        if(formula) {
            formula = formula.replace(/^\s*/, ''); // trim
            formula = formula.replace(/\s*$/, ''); // trim
            
            if(formula == 'X' || formula == 'x') {
                btnX.click();
            } else if(formula.match(/^\d+[dw]\d+/i)) {
                formula = formula.replace(/(\d+)w/gi, '$1d');
                roll(formula);
            } else {
                chat(clientid, formula);
            }
            $('#formula').val('');
        } else {
            toast('Keine W&uuml;rfelformel erkannt', 'Bitte gib zun&auml;chst eine W&uuml;rfelformel ein. Die Dokumentation &uuml;ber <a href="https://dice-roller.github.io/documentation/guide/notation/" target="_blank">die W&uuml;rfelformelnotation findest du hier</a>.');
        }
    });
    
    $('#btnX').click(function() {
        if(room == 'lobby') {
            toast('X-Karte', 'Die X-Karte kann in der Lobby nicht benutzt werden. Bitte wechsle mit deiner Gruppe in einen Raum. Das k&ouml;nnt ihr, indem ihr unter dem W&uuml;rfel-Log auf das Wort "lobby" doppelklickt und einen beliebigen Namen verwendet, den ihr untereinander absprecht.')
        } else {
            socket.emit('x');
        }
    });
    
    $('#btnRegisterPanel').click(function() {
        $(this).hide();
        $('#register').show();
    });
    
            
    $('.die').click(function() {
        $(this).css('transform', 'rotate(360deg)');
        var repeat=1;
        if($(this).data('repeat')) {
            repeat = $(this).data('repeat');
        }
        for(var r=0; r<repeat; r++) {
            roll($(this).data('formula'));
        }
    });
    $('img.die').hover(function() {
        $(this).css('transform', 'rotate(15deg) scale(1.1)');
    }, function() {
        $(this).css('transform', 'rotate(0deg) scale(1)');
    });
    
    $('img.dicetable').click(function() {
        $('#multidice').toggle();
    });
    
    $('.badge', '#log').click(function(){
        $('#room').dblclick();
    });

    $('#room .btnEdit').click(function(){
        $('#room').dblclick();
    });
    
    $('#room').dblclick(function(){
        toast('Den Raum wechseln', 'Um den Raum zu wechseln, kannst du einfach einen anderen Namen eingeben. Jede Person, die den gleichen Raumnamen verwendet, betritt diesen Raum. Du (und alle anderen im Raum) siehst nur noch die W&uuml;rfe der Personen im Raum.');
        
        $(this).data('room', room);
        $('#roomname', this).html('<input value="'+room+'">');
        $('input', this).focus();
        $('input', this).select();
        
        var that = $('#roomname', this);
        $('input', this).on('keypress', function(){
            if(event.which == 13) {
                $(this).blur();
            }
        });
        
        $('input', this).on('change blur', function(){
            socket.emit('leave', room);
            
            room = $(this).val();
            socket.emit('join', room);
            
            that.text(room);
        });
    });
    
    $('#sendRolls, #receiveRolls').click(function() {
        var icon='';
        var toast_message = '';
        if($(this).attr('id')=='sendRolls') {
            icon = 'fa-right-long';
            toast_message = 'Deine W&uuml;rfe sendest du jetzt ';
        } else {
            icon = 'fa-left-long';
            toast_message = 'W&uuml;rfe von anderen empf&auml;ngst du jetzt';
        }
        
        if($(this).hasClass('text-success')) {
            $(this).removeClass('text-success');
            $(this).addClass('text-danger');
            $(this).html('<i class="fa-solid fa-slash fa-stack-1x"></i>'
                    + '<i class="fa-solid ' + icon + ' fa-stack-1x"></i>');
            toast('Stumm schalten', toast_message + ' nicht mehr');
        } else {
            $(this).addClass('text-success');
            $(this).removeClass('text-danger');
            $(this).html('<i class="fa-solid ' + icon + ' fa-stack-1x"></i>');
            toast('Stumm schalten', toast_message + ' wieder');
        }
    });
    
    $('#connection').click(function(){
        toast('Verbindungsstatus', 'Dieses Icon zeigt dir an, ob eine Verbindung zum Chat-Server besteht. Wenn die Verbindung unterbrochen wird, wird sie in der Regel nach wenigen Sekunden automatisch wieder hergestellt.');
    });
    
    $('#registerid').click(function(event){
        event.stopPropagation();
        viewerModal($('.sendericons', $(this)));
    });
    
    // ----------------------------------------------------------------
    // socket.io
   
    function whois(register_id, callback) {
        socket.emit('whois', { 'register_id': register_id });
        rollbot.on('whois', function(data) {
            if(register[register_id]) {
                callback(register_id);
            }
        });
    }

    function chat(register_id, message) {
        $('#register').show();
        $('#btnRegisterPanel').hide();
        $('#log').show();
        $('#log').addClass('d-flex');

        const send = $('#sendRolls').hasClass('text-success');

        // first we add the line localy and replace it when server has answered
        const lineNr = socket.id+'_'+(++chatLineNr);
        const d = new Date();
        let time = (''+d.getHours()).padStart(2, '0') + ':' + (''+d.getMinutes()).padStart(2, '0') + ':' + (''+d.getSeconds()).padStart(2, '0');
        
        const sendIcon = '<span class="text-success fa-stack m-0 p-0">'
            + '<i class="fa-solid fa-right-long fa-stack-1x"></i>'
            + '</span>';

        const dontSendIcon = '<span class="text-danger fa-stack m-0 p-0">'
            + '<i class="fa-solid fa-slash fa-stack-1x"></i>'
            + '<i class="fa-solid fa-right-long fa-stack-1x"></i>'
            + '</span>';
        
        var chatline = $('<div class="roll" data-line-nr="'+lineNr+'"><small>'+time+'</small>' 
            + ( send ? sendIcon : dontSendIcon )
            + message 
            + '</div>'
        );
        $('#log .rolls').prepend(chatline);
        
        if(send) {
            socket.emit('chat', { 'sender': clientid, 'message' : message, 'register_id': register_id, 'line_nr': lineNr });    
        }
    }

    function roll(formula) {
        if(formula) {
            // roll.php is used as proxy to localhost
            $.ajax('roll.php?'+encodeURIComponent(formula)).done(function(data) {
                $('#diceroll').text(data);
                chat(clientid, data, true);
            })
            $('#log').show();
        }
    }
    
    // ----------------------------------------------------------------
    // UI

    function toast(title, text) {
        const toast = bootstrap.Toast.getOrCreateInstance($('#toast')[0])
        $('#toast .toast-title').html(title);
        $('#toast .toast-body').html(text);
        toast.show()
    }
    
    function viewerModal(elem) {
        const modal = new bootstrap.Modal('#modalViewer');
        var zoom = $(elem).clone();
        zoom.addClass('zoom');
        $('.modal-title', $('#modalViewer')).html(zoom.attr('title'));
        $('.modal-body .icons', $('#modalViewer')).html(zoom);
        modal.show();
    }
        
    function registeredIcons(register_id) {
        const sendericons = $('<div class="sendericons" data-color="'+register[register_id].color+'" title="'
                + register[register_id].icon[0]+' '+register[register_id].icon[1]+' '+register[register_id].icon[2]+'">' 
                + register[register_id].iconsvg[0] + register[register_id].iconsvg[1] + register[register_id].iconsvg[2] 
            + '</div>');
        
        sendericons.click(function(){
            viewerModal($(this));
        });
        
        return sendericons;
    }

});