<?
    header("Content-type: text/plain");
    
    $formula = htmlspecialchars(rawurldecode($_SERVER['QUERY_STRING']));
    
    $opts = [
        CURLOPT_SSL_VERIFYPEER  => false,
        CURLOPT_URL             => 'http://localhost:8123?'.rawurlencode($formula),
        CURLOPT_RETURNTRANSFER  => true,
        CURLOPT_CUSTOMREQUEST   => 'GET',
    ];
    
    $curl = curl_init();
    curl_setopt_array($curl, $opts);
    $response = curl_exec($curl);
    curl_close($curl);
    
    print $response;
?>