const io    = require('socket.io-client');
const fs    = require('fs');
const sane  = require('sanitize')();
const http  = require('http');

const clientid = 'pixels-server';

// connect to socket.io running on io.dnddeutsch.de
const socket = io("wss://io.dnddeutsch.de", {
    reconnectionDelayMax: 10000,
});

// register as sender
socket.on('connect', function(data) {
    socket.emit('register', { 'sender': clientid });    
    socket.emit('join', 'lobby');
});

// start server to send json from pixels app to
const server = http.createServer((request, response) => {
    if (request.method === 'POST') {
        let data = '';
        request.on('data', chunk => {
            data += chunk.toString();
        });
        request.on('end', () => {
            data = sane.value(JSON.parse(data), 'json');
            
            // send data to the console/log
            console.log('POST data:', data);
            
            const dieType = sane.value(data.dieType, 'str');
            const faceValue = sane.value(data.faceValue, 'int');
            const pixelName = sane.value(data.pixelName, 'str');
            
            // compose the chat message
            message = '1' + dieType + ': [' +faceValue+ '] ' + faceValue + ' (' + pixelName + ')';
            
            // send it to socket.io
            socket.emit('chat', { 'sender': clientid, 'message' : message });
            
            // afaik the pixels app doesn't do anything with the response
            response.end('ok');
        });
    } else {
        // if someone visits the endpoint with a browser, we read the howto-page
        fs.readFile('../web/pixels.html', 'utf8', function (err, data) {
            response.end(data);
        });
    }
});

server.listen(7935, () => {
    console.log('Server running on port 7935');
});