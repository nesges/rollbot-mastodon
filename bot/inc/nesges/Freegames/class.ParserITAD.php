<?
    namespace nesges\FreeGames;
    
    class ParserITAD extends Parser {

        function __construct() {
            parent::__construct();            
        }

        function parse($item, $data=[]) {
            global $config;
            
            $title = $item->get_title();
            if(preg_match('#\[giveaway\]\s+(.*?)\s-\sFREE on (.*)#', $title, $matches)) {
                $title = html_entity_decode(trim($matches[1]));
                $platform = html_entity_decode(trim(strtolower(preg_replace('/\s+/', '', $matches[2]))));
                $desc = html_entity_decode(trim(strip_tags($item->get_content())));
                $link = trim($item->get_permalink());
                $text = $title;
                
                if($title && $platform && !in_array($platform, ['gog', 'epicgames', 'steam', 'amazon', 'prime', 'humble']) ) {
                    // shorten status to <= $config['mastodon']['maxlen_status']
                    $linklen = 23;
                    $xtralen = 4 + 2 + 1; // " ..." + "\n\n" + " "
                    $tags = $data['tag'];
                    $tags[] = '#'.$platform;
                    $tags = join(" ", $tags);
                    $text = $this->shorten($text, $config['mastodon']['maxlen_status'] - (isset($tags) ? strlen($tags) : 0) -$xtralen -$linklen);
                
                    return [
                        'rss'       => $item->get_feed()->get_title(),
                        'guid'      => $item->get_id(),
                        'text'      => $text."\n\n".$link." ".$tags,
                        'img'       => null,
                        'imgtext'   => null,
                    ];
                }
            }
            return null;
        }
    }
?>