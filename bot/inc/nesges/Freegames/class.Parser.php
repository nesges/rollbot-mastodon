<?
    namespace nesges\FreeGames;
    
    class Parser {

        function __construct() {
            $opts = [
                'http'=> [
                    'method'=>"GET",
                    'header'=>"User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36\r\n"
                ]
            ];
            $this->context = stream_context_create($opts);
        }

        function parse($item, $data=[]) {
            return [
                'rss'       => $item->get_feed()->get_title(),
                'guid'      => $item->get_id(),
                'text'      => $this->shorten($item->get_description(), 500-25)."\n\n".$item->get_permalink(),
                'img'       => '',
                'imgtext'   => $this->shorten($item->get_description(), 1500),
            ];
        }
        
        // shorten $text to the last word before $len
        function shorten($text, $len) {
            $end = strpos(wordwrap($text, $len, "---WRAPHERE---"), "---WRAPHERE---");
            if($end) {
                $text = substr($text, 0, $end)." ...";
            }
            return $text;
        }

    }
?>