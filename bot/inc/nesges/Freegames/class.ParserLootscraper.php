<?
    namespace nesges\FreeGames;
    
    class ParserLootscraper extends Parser {

        function __construct() {
            parent::__construct();
        }

        function parse($item, $data=[]) {
            // parses LootScraper RSS
            global $config;
            
            $content = html_entity_decode(trim($item->get_content()));
            
            $img = '';
            if(preg_match('#<img\s+src="(.*?)"#', $content, $matches)) {
                $img = trim($matches[1]);
            }
            
            $desc = '';
            if(preg_match('#<li><b>Description:</b>(.*?)</li>#s', $content, $matches)) {
                $desc = trim($matches[1]);
            }
            
            $title = preg_replace('#.*?\(Game\) - #', '', $item->get_title());
            $link = trim($item->get_permalink());
            
            $text = "$title\n\n$desc";
            
            if($text) {
                // shorten status to <= $config['mastodon']['maxlen_status']
                $linklen = 23;
                $xtralen = 4 + 2 + 1; // " ..." + "\n\n" + " "
                $tags = join(" ", $data['tag']);
                $text = $this->shorten($text, $config['mastodon']['maxlen_status'] - (isset($tags) ? strlen($tags) : 0) -$xtralen -$linklen);
            
                // shorten imgtext to <= $config['mastodon']['maxlen_imgdesc']
                $imgtext = preg_replace("#(\n)\s*#", "$1", trim(strip_tags($item->get_content())));
                $imgtext = $this->shorten($imgtext, $config['mastodon']['maxlen_imgdesc'] - $xtralen - 25);
            
                return [
                    'rss'       => $item->get_feed()->get_title(),
                    'guid'      => $item->get_id(),
                    'text'      => $text."\n\n".$link." ".$tags,
                    'img'       => $img,
                    'imgtext'   => $imgtext,
                ];
            }
            return null;
        }

    }
?>