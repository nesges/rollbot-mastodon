<?
    namespace nesges;

    class Mastodon {
        function __construct($domain, $access_token='') {
            $this->domain = $domain;
            $this->api = 'https://'.$domain;
    
            if(isset($access_token)) {
                $this->access_token = $access_token;
            }

            $opts = [
                'http'=> [
                    'method'=>"GET",
                    'header'=>"User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36\r\n"
                ]
            ];
            $this->context = stream_context_create($opts);
        }
        
        function create_application($name, $website="") {
            // step 1: create_application
            // receive client_id, client_secret
            // next: call authorize_user with client_id, client_secret
            return $this->request('POST', '/api/v1/apps', [
                "client_name"   => $name,
                "redirect_uris" => "urn:ietf:wg:oauth:2.0:oob",
                "website"       => $website,
                "scopes"        => "read write",
            ]);
        }
        
        function authorize_user_url($client_key, $client_secret) {
            // step 2: authorize_user
            // receive code
            // next: call obtain_token with client_id, client_secret, code
            
            return $this->api.'/oauth/authorize?'.http_build_query([
                "response_type" => "code",
                "redirect_uri"  => "urn:ietf:wg:oauth:2.0:oob",
                "client_id"     => $client_key,
                "scope" => "read write",
            ]);
        }
        function authorize_user($client_key, $client_secret) {
            header('Location: '.$this->authorize_user_url($client_key, $client_secret));
        }
        
        function obtain_token($client_key, $client_secret, $code ) {
            // step 3: obtain_token
            // receive token
            return $this->request('POST', '/oauth/token', [
                "grant_type"    => "authorization_code",
                "redirect_uri"  => "urn:ietf:wg:oauth:2.0:oob",
                "client_id"     => $client_key,
                "client_secret" => $client_secret,
                "code"          => $code,
            ]);
        }
        
        function post_status($text = "", $visibility = "public", $media = [], $in_reply_to_id=""){
            return $this->request('POST', '/api/v1/statuses', [
                "status"        => $text,
                "visibility"    => $visibility,
                "media_ids"     => is_array($media) ? $media : explode(" ", $media),
                "in_reply_to_id"=> $in_reply_to_id,
            ]);
        }
        
        function delete_status($id) {
            return $this->request('DELETE', '/api/v1/statuses/'.$id, []);
        }
        
        function create_attachment($filePath, $description="") {
            if(!realpath($filePath)) {        
                $filecontent = file_get_contents($filePath, false, $this->context);
                $filePath = tempnam('cache', 'image-');
                file_put_contents($filePath, $filecontent);
                $is_tempfile = true;
            } else {
                $is_tempfile = false;
            }
            
            $mimeType = mime_content_type($filePath);
            $curlFile = curl_file_create( $filePath, $mimeType, 'file' );
            
            $response = $this->request('POST', '/api/v1/media', [
                'access_token'  => $this->access_token,
                'file'          => $curlFile,
                'description'   => $description,
            ]);
      
            if($is_tempfile) {
                unlink($filePath);
            }
     
            return $response;
        }
        
        function notifications($since_id=0, $limit=20, $types=[]) {
            return $this->request('GET', '/api/v1/notifications', [
                'since_id' => $since_id,
                'limit' => $limit,
                'types' => $types,
            ]);
        }
        
        function favourited_by($status_id) {
            return $this->request('GET', '/api/v1/statuses/'.$status_id.'/favourited_by');
        }
        
        function favourite($status_id) {
            return $this->request('POST', '/api/v1/statuses/'.$status_id.'/favourite');
        }
        
        function request($type, $endpoint, $payload=[]) {
            if(isset($this->access_token)) {
                $payload['access_token'] = $this->access_token;
            }
     
            // 2nd level arrays aren't resolved by curl itself, so...
            foreach($payload as $key => $value) {
                if(is_array($value)) {
                    foreach($value as $item) {
                        $payload[$key.'[]']=$item;
                    }
                    unset($payload[$key]);
                }
            }
      
            if($type=='GET') {
                $opts = [
                    CURLOPT_SSL_VERIFYPEER  => false,
                    CURLOPT_HTTPHEADER => [
                      'Accept'            => '*/*',
                      'Authorization'     => isset($this->access_token) ? 'Bearer '.$this->access_token : '',
                      'Idempotency-Key'   => uniqid(),
                    ],
                    CURLOPT_URL             => preg_replace('#//#', '/', $this->api.$endpoint).'?'.http_build_query($payload),
                    CURLOPT_RETURNTRANSFER  => true,
                ];
            } else {
                $opts = [
                    CURLOPT_SSL_VERIFYPEER  => false,
                    CURLOPT_HTTPHEADER => [
                      'Content-Type'      => 'multipart/form-data',
                      'Accept'            => '*/*',
                      'Authorization'     => isset($this->access_token) ? 'Bearer '.$this->access_token : '',
                      'Idempotency-Key'   => uniqid(),
                    ],
                    CURLOPT_URL             => preg_replace('#//#', '/', $this->api.$endpoint),
                    CURLOPT_RETURNTRANSFER  => true,
                    CURLOPT_POSTFIELDS      => $payload,
                ];
            }
            if($type != 'POST') {
                $opts[CURLOPT_CUSTOMREQUEST] = $type;
            }
            
            $curl = curl_init();
            curl_setopt_array($curl, $opts);
            $response = curl_exec($curl);
            curl_close($curl);
     
            return json_decode($response, true);
        }
    }
?>