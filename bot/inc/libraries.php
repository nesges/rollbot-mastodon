<? 
    // composer
    if(file_exists("vendor/autoload.php")) {
        require_once "vendor/autoload.php";
    }
    
    // if you don't use composer than have a look at composer.json, 
    // install these modules somewhere and include them here
?>