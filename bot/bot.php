<?
    header("Content-type: text/plain");
    
    // secure against multiple invocation
    // but ignore sem after 1 hour
    if(file_exists('running.sem')) {
        if(filemtime('running.sem')>time()-60*60) {
            unlink('running.sem');
        } else {
            exit;
        }
    }
    touch('running.sem');
    
    // ini_set('display_errors', 1);
    // ini_set('display_startup_errors', 1);
    // error_reporting(E_ALL);
    
    require_once "config.php";
    require_once "inc/libraries.php";
    require_once "inc/nesges/class.Mastodon.php";

    // connect to mastodon
    $mastodon = new \nesges\Mastodon($config['mastodon']['domain'], $config['mastodon']['access_token']);
    
    // connect to mysql
    $db = new mysqli($config['database']['host'], $config['database']['user'], $config['database']['pass'], $config['database']['db']);
    if (mysqli_connect_errno()) {
        $response[error]="database connect failed";
    } else {
        $db->query("SET NAMES utf8");
    }
    
    // select all known mentions, aka mentions we have already replied to
    $known = [];
    $mention_id=0;
    $res = $db->query("select mention_id from mastodon_rolls order by id desc limit 50"); // todo: limit 50 is rather arbitrary, maybe change to age ~1 hour
    while(list($id) = $res->fetch_row()) {
        $known[] = $id;
    }
    $max_mention_id = max($known, 0);
    
    // get 20 mentions from mastodon
    $mentions = $mastodon->notifications($max_mention_id, 20, ['mention']);
    foreach($mentions as $mention) {
        $mention_id = $mention['id'];
        $status_id = $mention['status']['id'];
        $status_url = $mention['status']['url'];
        $created_at = strtotime($mention['created_at']);    // max age of mentions is 1 h, see below
        $visibility = $mention['status']['visibility'];     // reply using the same visibility
        if($visibility == 'public') {
            $visibility = 'unlisted'; // but never reply publicly
        }
        
        $formula = '';
        $diceroll = '';
        
        // ignore mentions whe already know or that are older than 1 hour
        if(!in_array($mention_id, $known) && $created_at > time()-60*60) {
            $favourite = $mastodon->favourite($status_id);
            
            // ignore, if that status has been favourited before
            // if(strtotime($favourite['created_at']) > time()-10) { // todo: this seems to be created_at of the status, not the favourite
                $status_content = htmlspecialchars(trim(strip_tags($mention['status']['content'])));
                
                // mandatory format: 
                // (\d+)[dDwW](\d+)                     => XdY 
                // [a-zA-Z0-9*%/+-.!<>=]*               => optionally followed by a finite range of characters
                // (\s+#\s? ... )?                      => optionally followed by a comment separated by at least one space
                //    [a-zA-ZäöüßÄÖÜ0-9_+-]+     => comment can contain only alphanumerics and _+-
                //    (\s|$)                            => comment must be followed by space or end of string
                // this may prohibit some formulae that roller would understand but mitigates the risk 
                // of executing arbitrary code when $formula is handed over to exec

                print $status_content."\n";

                if(preg_match('@(((\d+)[dDwW]([0-9%F]+)[a-zA-Z0-9*%/!<>=.+-]*)+(\s+#\s?[a-zA-ZäöüßÄÖÜ0-9_+-]+(\s|$))?)@', $status_content, $matches)) {
                    $formula = $matches[1];
                    $first_dice_count = $matches[3];
                    $first_dice_sides = $matches[4];
                    
                    if($first_dice_sides==0) {
                        $diceroll = 'Es tut mir leid, aber Würfel mit weniger als einer Seite kann ich nicht werfen :Fehlschlag:';
                    } else if($first_dice_count>999) {
                        $formula = 'error';
                        $diceroll = 'Es tut mir leid, aber ich kann nicht mehr als 999 Würfel auf einmal werfen :Fehlschlag:';
                    } else if($first_dice_count<1) {
                        $formula = 'error';
                        $diceroll = 'Es tut mir leid, aber ich muss mindestens einen Würfel werfen :Fehlschlag:';
                    } else {
                        // replace w with d
                        $formula = preg_replace('/\b(\d+)[dDwW]/i', '$1d', $formula);
                        print "roll: ".$formula."\n";
                        $diceroll = roll($formula);
                        
                        //if($diceroll) {
                            if(in_array($first_dice_sides, [4,6,8,10,12,20,100])) {
                                $diceroll = ':W'.($first_dice_sides==100?10:$first_dice_sides).': '.$diceroll;
                            }
                            if(strlen($diceroll) > $config['mastodon']['maxlen_status']) {
                                $diceroll = preg_replace('/,\s/', ',', $diceroll);
                                if(strlen($diceroll) > $config['mastodon']['maxlen_status']) {
                                    $diceroll = preg_replace('/\[.*?\]/', '[sehr viele Würfel]', $diceroll);
                                }
                            }
                        //} else {
                            //$formula = 'error';
                            //$diceroll = 'Es tut mir leid, aber da ist etwas schief gelaufen und ich habe kein Ergebnis für dich :Fehlschlag:  Ich informiere @dnddeutsch@pnpde.social';
                        //}
                    }
                } else {
                    if(preg_match('/dank|thank|thx|<3|gott vergelt|vergelt.?s gott|merci/i', $status_content)) {
                        $formula = 'thx';
                        $diceroll = $config['reply']['thx'][rand(0, count($config['reply']['thx'])-1)];
                    } else {
                        if(substr_count($status_content, '@')>1) {
                            $formula = 'ignore';
                        } else {
                            $formula = 'error';
                            $diceroll = 'Es tut mir leid, aber ich habe keine gültige Würfelformel erkannt :Fehlschlag:'."\n\n".'Eine einfache Formel ist zum Beispiel "2W20" für zwei zwangzigseitige Würfel, oder "4W6" für vier sechsseitige. Die Dokumentation zur Würfelnotation findest du unter https://dice-roller.github.io/documentation/guide/notation/';
                        }
                    }
                }
                
                print $formula." ".$diceroll."\n";
                
                // reply           
                if($formula != 'ignore') {
                    $result = $mastodon->post_status(substr($diceroll, 0, $config['mastodon']['maxlen_status']), $visibility, [], $status_id);
                } else {
                    print $mention_id."\n";
                    
                    $stmt = $db->prepare("insert into mastodon_rolls (mention_id, status_id, status_url, formula) values (?, ?, ?, ?) on duplicate key update formula=?");
                    $stmt->bind_param("sssss", $mention_id, $status_id, $status_url, $formula, $formula);
                    $stmt->execute();
                    $stmt->close();
                }
                if(isset($result['id'])) {
                    $reply_id = $result['id'];
                    $reply_url = $result['url'];
                    
                    $stmt = $db->prepare("insert into mastodon_rolls (mention_id, status_id, status_url, formula, diceroll, reply_id, reply_url) values (?, ?, ?, ?, ?, ?, ?)");
                    $stmt->bind_param("sssssss", $mention_id, $status_id, $status_url, $formula, $diceroll, $reply_id, $reply_url);
                    $stmt->execute();
                    $stmt->close();
                } else {
                    // when does this happen?
                    print_r($result);
                    
                    $formula = 'CRITICAL ERROR: '.json_encode($result);
                    $stmt = $db->prepare("insert into mastodon_rolls (mention_id, status_id, status_url, formula, diceroll, reply_id, reply_url) values (?, ?, ?, ?, ?, ?, ?)");
                    $stmt->bind_param("sssssss", $mention_id, $status_id, $status_url, $formula, $diceroll, $reply_id, $reply_url);
                    $stmt->execute();
                    $stmt->close();
                }
            // } else {
            //     // status was favourited before
            //     // this could happen if we were unable to save to mysql before
            //     $formula = 'favourited before';
            // 
            //     $stmt = $db->prepare("insert into mastodon_rolls (mention_id, status_id, status_url, formula) values (?, ?, ?, ?) on duplicate key update formula=?");
            //     $stmt->bind_param("sssss", $mention_id, $status_id, $status_url, $formula, $formula);
            //     $stmt->execute();
            //     $stmt->close();
            // }
        }
    }
    
    unlink('running.sem');
    
    function roll($formula) {
        global $config;

        $opts = [
            CURLOPT_SSL_VERIFYPEER  => false,
            CURLOPT_URL             => 'http://'.$config['server.js']['host'].':'.$config['server.js']['port'].'?'.rawurlencode($formula),
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_CUSTOMREQUEST   => 'GET',
        ];

        $curl = curl_init();
        curl_setopt_array($curl, $opts);
        $response = curl_exec($curl);
        curl_close($curl);
        if($response) {
            return $response;
        }

        // fallback if server doesn't respond / is down
        if(isset($config['cli']['roller'])) {
            if(!file_exists('mail.notification') || filemtime('mail.notification') < time()-$config['server.js']['notify']['interval']) {
                if(isset($config['server.js']['notify']['mail']) && $config['server.js']['notify']['mail'] != 'your@ema.il') {
                    mail($config['server.js']['notify']['mail'], 'rollbot server.js down', 'server.js not reached on http://'.$config['server.js']['host'].':'.$config['server.js']['port'].'?'.$formula);
                    touch('mail.notification');
                }
            }
            // check if shellsafe
            if(preg_match('@((\d+)[dDwW]([0-9%F]+)[a-zA-Z0-9*%/!<>=.+-]*(\s+#\s?[a-zA-ZäöüßÄÖÜ0-9_+-]+(\s|$))?)@', $formula, $matches)) {
                $formula = escapeshellcmd($formula);
                $roller = $config['cli']['roller'];
                return `$roller "$formula"`;
            }
        }

        return false;
    }
?>
