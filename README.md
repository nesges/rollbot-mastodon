# rollbot

Utilizes [RPG Dice Roller](https://github.com/dice-roller/rpg-dice-roller) as a lightweight mastodon bot in PHP and Node.js



## Installation

* Setup a mastodon app on DOMAIN and get an ACCESS_TOKEN
* Setup a MySQL database
* Choose a port for server.js
* you'll need composer, node and npm 

```
git clone $REPOSITORY
chown -R www-data.www-data $REPOSITORY_DIR
```

### rollbot-bot
```
cd bot
composer install
```

### rollbot-server
```
cd server
npm install
```

### rollbot-web

this is the code of https://rollbot.dnddeutsch.de. Feel free to copy and modify as needed

### Dice-Roller CLI (optional)
```
npm install -g @dice-roller/cli
```
bot uses `roller` as fallback, when server.js doesn't respond



## Configuration

### config.php

```
cp config.sample.php bot/config.php
ln -s bot/config.php web/config.php
```

Set $config values for mastodon, database and server.js in bot/config.php

```
export ROLLBOT_HOST=$HOST
export ROLLBOT_PORT=$PORT
```

Use HOST and PORT from $config['server.js']



## Usage
```
node server/server.js
php bot/bot.php
```
run bot.php via cron and make server.js permanent (e.g. with systemd, see rollbot.service.sample)

## Test
```
curl http://$HOST:$PORT?2d20
```
Should output something like `2d20: [8, 13] = 21`