const http = require("http");
const { DiceRoller } = require('@dice-roller/rpg-dice-roller');

const host = process.env.ROLLBOT_HOST;
const port = process.env.ROLLBOT_PORT;
const roller = new DiceRoller();

const requestListener = function (request, response) {
    const query = decodeURIComponent(request.url.replace(/.*\?/, ''));
    let roll = roller.roll(query);
    response.setHeader("Content-Type", "text/plain");
    response.writeHead(200);
    response.end(`${roll}`);
};

const server = http.createServer(requestListener);
server.listen(port, host, () => {
    console.log(`Server is rolling on http://${host}:${port}`);
});