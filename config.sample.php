<?
    $config = [
        'server.js' => [
            'host' => 'localhost',
            'port' => 8000,
            'notify' => [
                'mail' => 'your@ema.il',
                'interval' => 60*60*4, // 4 hours
            ]
        ],
        'cli' => [
            'roller' => '/usr/bin/roller', // empty, if you don't want to use roller cli as fallback option
        ],
        'mastodon' => [
            'domain'            => '',
            'access_token'      => '', 
            'user_id'           => '', // lookup with https://prouser123.me/mastodon-userid-lookup/
            'maxlen_status'     => 500, // default on most mastodon servers
            'maxlen_imgdesc'    => 1500,// default on most mastodon servers
        ],
        'database' => [
            'db'   => '',
            'host' => '',
            'user' => '',
            'pass' => '',
        ],
        'reply' => [
            'thx' => [ 'Gerne!', 'Stets zu Diensten', 'Gerne wieder!', 'Immer gern', 'Jederzeit!', 'Schön, wenn man helfen kann', 'Es war mir ein Vergnügen' ]
        ],
        'salt' => '' // (optional) some random string to salt md5 strings
    ]
?>